<?php
 require('./correios.php');
 $correios = new Correios; // Chama a classe Correios

 // "Carrinho" de exemplo
 $produtos = array(
     array('nome' => 'Produto 1', 'valor' => 23.59, 'peso' => 2, 'altura' => 15, 'largura' => 15, 'comprimento' => 15),
     array('nome' => 'Produto 2', 'valor' => 30.00, 'peso' => 1, 'altura' => 10, 'largura' => 12, 'comprimento' => 20),
     array('nome' => 'Produto 3', 'valor' => 44.99, 'peso' => 0.3, 'altura' => 10, 'largura' => 15, 'comprimento' => 25),
 );

 // Para cada produto no carrinho, chama as funções para definir o valor declarado, peso e dimensões do pedido
 foreach($produtos as $produto) {
     $correios->setPeso($produto['peso']); // Em Kg
     $correios->setValor($produto['valor']); // Em Reais
     $correios->setDimensoes($produto['altura'], $produto['largura'], $produto['comprimento']); // Em centímetros
 }

 // Define os CEPS de Origem e Destino
 $correios->setCepDestino('84261-390');
 $correios->setCepOrigem('13660-000');

 // Envia os dados para o WebService dos Correios
 $servicos = $correios->consultaCorreio();
?>

<!-- Informações tratadas -->
<meta charset="utf-8">
<body>
    <ul>
        <?php foreach($servicos as $servico){ ?>
            <li><b><?=$correios->getServiceByCode($servico->Codigo); ?></b> - R$ <?=$servico->Valor;?> (<?=$servico->PrazoEntrega;?> dias úteis)</li>
        <?php } ?>
    </ul>
</body>
