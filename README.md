**Módulo Orientado a Objetos do WebService dos Correios**

O arquivo index.php contém somente dados de exemplo. Para integração ao seu sistema, faça o download do arquivo correios.php, requisite o arquivo em sua página de cálculo de frete e envie as informações necessárias para o cálculo (Conforme orientação no arquivo index.php).

---

## Licença

O repositório está disponível em código aberto, estando disponível para livre utilização em sistemas comerciais ou não sem a necessidade de créditos aos desenvolvedores.

---

## Desenvolvido por Gustavo Barbosa
