<?php

class Correios
{

    private $valor_max = 10000;    // máximo valor declarado, em reais

    private $altura_max = 105;        // todas as medidas em cm
    private $largura_max = 105;
    private $comprimento_max = 105;

    private $altura_min = 2;
    private $largura_min = 11;
    private $comprimento_min = 16;

    private $soma_dim_max = 200;    // medida máxima das somas da altura, largura, comprimento

    private $peso_max = 30;    // padrão, em kg

    private $peso_max_pac_sedex = 30;    // em kg
    private $peso_max_esedex = 15;    // em kg
    private $peso_max_sedex10_hoje = 10;    // em kg

    private $peso_min = 0.300;            // em kg

    private $nCdServico = array();
    private $servicos = array();

    private $cep_destino;
    private $cep_origem;

    private $contrato_codigo = '';
    private $contrato_senha = '';

    private $mensagem_erro = array();

    private $peso_pedido = 0;

    private $valorPedido = 0;

    private $altura_pedido;
    private $largura_pedido;
    private $comprimento_pedido;

    private $diametro_pedido;

    // Código dos serviços que precisam ser consultados
    private $cServicos = array(
        '40010',
        '41106'
    );

    // Utilize este array como base para preencher o array acima. Também utilize para recuperar o nome do serviço a partir do código
    private $correios = array(
        'SEDEX' => '40010',
        '40010' => 'SEDEX',

        'SEDEX a Cobrar' => '40045',
        '40045' => 'SEDEX a Cobrar',

        'SEDEX a Cobrar - contrato' => '40126',
        '40126' => 'SEDEX a Cobrar - contrato',

        'SEDEX 10' => '40215',
        '40215' => 'SEDEX 10',

        'SEDEX Hoje' => '40290',
        '40290' => 'SEDEX Hoje',

        'SEDEX - contrato 1' => '40096',
        '40096' => 'SEDEX - contrato 1',

        'SEDEX - contrato 2' => '40436',
        '40436' => 'SEDEX - contrato 2',

        'SEDEX - contrato 3' => '40444',
        '40444' => 'SEDEX - contrato 3',

        'SEDEX - contrato 4' => '40568',
        '40568' => 'SEDEX - contrato 4',

        'SEDEX - contrato 5' => '40606',
        '40606' => 'SEDEX - contrato 5',

        'PAC' => '41106',
        '41106' => 'PAC',

        'PAC - contrato' => '41068',
        '41068' => 'PAC - contrato',

        'e-SEDEX' => '81019',
        '81019' => 'e-SEDEX',

        'e-SEDEX Prioritario' => '81027',
        '81027' => 'e-SEDEX Prioritario',

        'e-SEDEX Express' => '81035',
        '81035' => 'e-SEDEX Express',

        'e-SEDEX grupo 1' => '81868',
        '81868' => 'e-SEDEX grupo 1',

        'e-SEDEX grupo 2' => '81833',
        '81833' => 'e-SEDEX grupo 2',

        'e-SEDEX grupo 3' => '81850',
        '81850' => 'e-SEDEX grupo 3'
    );

    // Função para recuperar o nome do serviço pelo código
    public function getServiceByCode($codigo){
        echo $this->correios["$codigo"];
    }

    // Soma o valor Peso enviado ao já definido nas outras chamadas da mesma função
    public function setPeso($peso){
        $peso = round($peso);
        $this->peso_pedido += $peso;

        if($this->peso_pedido <= $this->peso_max){
            return true;
        }else{
            return false;
        }
    }

    // Soma o valor Valor enviado ao já definido nas outras chamadas da mesma função
    public function setValor($valor){
        $this->valorPedido += round($valor);

        if($this->valorPedido > $this->valor_max){
            return false;
        }else{
            return true;
        }
    }

    // Define o CEP Destino
    public function setCepDestino($cep){
        $cep = preg_replace('/[^0-9]/', '', $cep);
        $this->cep_destino = $cep;
    }

    // Define o CEP de Origem
    public function setCepOrigem($cep){
        $cep = preg_replace('/[^0-9]/', '', $cep);
        $this->cep_origem = $cep;
    }

    // Define as dimensões do pedido
    public function setDimensoes($alt, $lar, $comp){
        $this->altura_pedido += $alt;
        $this->largura_pedido += $lar;
        $this->comprimento_pedido += $comp;

        if($this->altura_pedido > $this->altura_max || $this->largura_pedido > $this->largura_max || $this->comprimento_pedido > $this->comprimento_max){
            return false;
        }else{
            return true;
        }
    }

    // Define o diametro total do pedido
    public function setDiametro($alt, $larg, $comp){
        $this->diametro_pedido = $alt + $larg + $comp;

        if($this->diametro_pedido > $this->soma_dim_max){
            return false;
        }else{
            return true;
        }
    }

    // Envia as informações definidas para o WebService afim de realizar a consulta
    public function consultaCorreio(){
        if(!$this->setDiametro($this->altura_pedido, $this->largura_pedido, $this->comprimento_pedido)){
            return false;
        }

        $servicos = implode(",", $this->cServicos);

        // URL de Consulta dos Correios entregue à variavel $correios
        $correios ="http://ws.correios.com.br/calculador/CalcPrecoPrazo.aspx?"
            ."nCdEmpresa=&"
            ."sDsSenha=&"
            ."sCepOrigem=$this->cep_origem&"
            ."sCepDestino=$this->cep_destino&"
            ."nVlPeso=$this->peso_pedido&"
            ."nCdFormato=1&"
            ."nVlComprimento=$this->comprimento_pedido&"
            ."nVlAltura=$this->altura_pedido&"
            ."nVlLargura=$this->largura_pedido&"
            ."sCdMaoPropria=n&"
            ."nVlValorDeclarado=$this->valorPedido&"
            ."sCdAvisoRecebimento=n&"
            ."nCdServico=$servicos&"
            ."nVlDiametro=$this->diametro_pedido&"
            ."StrRetorno=xml";

        $dados_correios = simplexml_load_file($correios);

        $erros = $dados_correios->xpath('cServico/Erro');

        // Retorna as informações em formato de objeto para o sistema, que deve tratá-la conforme a necessidade
        return $dados_correios;

    }
}

?>